<!--
<h3>Home</h3>
<p>Displays the station configuration with the Valco calve postition and the electrovalves states.</p>
-->



<div id="Help_MainParagraph" >
	<h2>
		Software datasheets
	</h2>
	
	This software is composed by three separated programs working togethers. 
	The <b>Web application</b> program gives acces to the user to the controls tools,
	the <b>Hardware Controller</b> program handles the hardware,
	and the <b>Bridge</b> program links the two previous programs.
	<br/>
	<br/>
	<br/>
	<div id="DatasheetsToDownload">
		
		<h3>Global datasheet - overview of the software</h3>
		
		<div id="GlobalDatasheets">
			<div class="DocumentToDownload">
				<h4>
					Scheduler - Full Software datasheet
				</h4>
				<a href="https://sharebox.lsce.ipsl.fr/index.php/s/ayxn4xyD6moVhWI" target="_blank">
					<IMG SRC="Assets_Pictures/Datasheet-entire_program.JPG" class="Datasheet_Miniature">
				</a>
			</div>
			<div class="DocumentToDownload">
				<h4>
					Interface - Tables (Excel)
				</h4>
				<a href="Assets_Documents/Interfaces.xlsx" target="_blank">
					<IMG SRC="Assets_Pictures/ExcelIcon.JPG" class="Datasheet_Miniature">
				</a>
			</div>
			<div class="DocumentToDownload">
				<h4>
					Interface - Schematic (PDF)
				</h4>
				<a href="Assets_Documents/Full_software_interfaces.pdf" target="_blank">
					<IMG SRC="Assets_Pictures/Full_Software_Interfaces.JPG" class="Datasheet_Miniature">
				</a>
			</div>
		</div>

		<br/>
		<br/>
		
		<h3>Specific datasheets about the three software programs </h3>
		
		<div id="SubDatasheets">
			<div class="DocumentToDownload">			
				<h4>
					Program 1 - Web Application
				</h4>
				<a href="https://sharebox.lsce.ipsl.fr/index.php/s/UHf0FGWKo0t3gc4" target="_blank">
					<IMG SRC="Assets_Pictures/Datasheet-WebApplication.JPG" class="Datasheet_Miniature">
				</a>
			</div>
			
			<div class="DocumentToDownload">	
				<h4>
					Program 2 - Bridge
				</h4>
				<a href="https://sharebox.lsce.ipsl.fr/index.php/s/OpmiLogNKlJ2vcI" target="_blank">
					<IMG SRC="Assets_Pictures/Datasheet-Bridge.JPG" class="Datasheet_Miniature">
				</a>
			</div>
			
			<div class="DocumentToDownload">	
				<h4>
					Program 3 - Hardware Controller
				</h4>
				<a href="https://sharebox.lsce.ipsl.fr/index.php/s/W3nuq3A31jEDAFc" target="_blank">
					<IMG SRC="Assets_Pictures/Datasheet-HardwareController.JPG" class="Datasheet_Miniature">
				</a>
			</div>
		</div>
	</div>
	
	<br/>
	<br/>
	<br/>
	<br/>
	<h2>Software source code</h2>
	
	<p>
		The JAVASCRIPT source code is stored on Gitlab on the link below:
		<br/>
		<a href="https://gitlab.in2p3.fr/iban.fernandez/javascript-graphical-interface/-/tree/master" target="_blank">
		https://gitlab.in2p3.fr/iban.fernandez/javascript-graphical-interface/-/tree/master</a>
	</p>
	<br/>
	<br/>
	<p>
		The web site source code is stored on Gitlab on the link below:
		<br/>
		<a href="https://gitlab.in2p3.fr/iban.fernandez/html-web-site-scheduler" target="_blank">
		https://gitlab.in2p3.fr/iban.fernandez/html-web-site-scheduler</a>
	</p>

	<br/>
	<br/>
	<br/>
	<br/>
	<h2>Developper Ressources</h2>
	
	<div id="SubDatasheets">
			<div class="DocumentToDownload">			
				<h4>
					Programming environment installation
				</h4>
				<a href="https://sharebox.lsce.ipsl.fr/index.php/s/lty5VngqR92R9EB" target="_blank">
					<IMG SRC="Assets_Pictures/Programming_Environment.JPG" class="Datasheet_Miniature">
				</a>
			</div>
			
			<div class="DocumentToDownload">	
				<h4>
					Software integration inside the programming environment
				</h4>
				<a href="" target="_blank">
					<IMG SRC="" class="Datasheet_Miniature">
				</a>
			</div>
	</div>
	
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
</div>