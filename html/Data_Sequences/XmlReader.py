#!/usr/bin/python3
print ("Content-Type: text/html\n")

import xml.etree.ElementTree as ET

# Creation of an object named tree by instanciate the class ElementTree()
tree = ET.ElementTree()
# Call a function of tree that parse the desired file
tree.parse('SequenceTest.xml')
# Find the tree's root
root = tree.getroot()

i = 1

for step in root.iter('step'):
    print('Step %d' %i)
    print('position')
    print(step.find('position').text)
    print('duration')
    print(step.find('duration').text)
    i = i + 1
