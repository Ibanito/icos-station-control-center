<!DOCTYPE html>
<!-- This page is the main page of the Valco Scheduler -->
<!-- This page is never reloaded -->
<!-- The five tabs contain the five main graphical interfaces -->

<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="style.css" />
		<link rel="stylesheet" href="Section_SequencesBuilder/sequencesBuilder.css" >
		<link rel="stylesheet" href="Section_Help/Help.css" >
		<title>VALCOBOX BIG BOSS BOX 2</title>
	</head>

	<body>
		<header>
			<div id="header">
				<div>
					<p id="mainTitle"><b>ICOS Station control centre</b></p>
				</div>
				<div>
					<IMG SRC="Assets_Pictures/LSCE_blanc.png">
					<IMG SRC="Assets_Pictures/ICOS_blanc.png">
				</div>
			</div>
		</header>
		
		<!-- Tab links -->
		<div class="tab">
			<button class="tablinks active" onclick="openCity(event, 'Home')">Home</button>
			<button class="tablinks" onclick="openCity(event, 'Configuration')">Station configuration</button>
			<button class="tablinks" onclick="openCity(event, 'Manual')">Manual control</button>
			<button class="tablinks" onclick="openCity(event, 'Automatic')">Automatic scheduling</button>
			<button class="tablinks" onclick="openCity(event, 'Sequences')">Sequences building</button>
			<button class="tablinks" onclick="openCity(event, 'Help')">Help</button>
		</div>

		<!-- Tab content -->
		<div id="Home" class="tabcontent" style="display: block;">
			<?php
			  include('Section_Home/Home.php');   // Nous appelons notre menu
			?>
		</div>

		<div id="Configuration" class="tabcontent">
			<?php
			  include('Section_StationConfiguration/StationConfiguration.php');   // Nous appelons notre menu
			?>
		</div>

		<div id="Manual" class="tabcontent">
			<?php
			  include('Section_ManualControl/Manual.php');   // Nous appelons notre menu
			?>
		</div> 
		
		<div id="Automatic" class="tabcontent">
			<?php
			  include('Section_AutomaticMode/Automatic.php');   // Nous appelons notre menu
			?>
		</div> 
		
		<div id="Sequences" class="tabcontent">
			<?php
			  include('Section_SequencesBuilder/Sequences.php');   // Nous appelons notre menu
			?>
		</div>
		
		<div id="Help" class="tabcontent">
			<?php
			  include('Section_Help/Help.php');   // Nous appelons notre menu
			?>
		</div>
		
		<!-- JAVA SCRIPT -->
		<!-- This scirpt is used to browse through tabs. -->
		<script type="text/javascript" src="BrowseThroughMainPageTabs.js"></script>

		<!-- End of the toogle tab -->
		
		<!--
		<p>Python test script:</p>		
		<FORM action="pythonScript.py" method="POST" target="hidden-form">
			<INPUT TYPE="submit" NAME="send" VALUE="Exécuter le script">
		</FORM>
		<IFRAME style="display:none" name="hidden-form"></IFRAME>
		-->
		
		<style>
			#javascriptVersion
			{
				color: #00ff00;
				font-family: "Courier New";
			}
		</style>
		<p id="javascriptVersion">
			<script type="text/javascript">
			var jsver = 1.0;
			</script>
			<script  language="Javascript1.1">
			jsver = 1.1;
			</script>
			<script language="Javascript1.2">
			jsver = 1.2;
			</script>
			<script  language="Javascript1.3">
			jsver = 1.3;
			</script>
			<script   language="Javascript1.4">
			jsver = 1.4;
			</script>
			<script  language="Javascript1.5">
			jsver = 1.5;
			</script>
			<script  language="Javascript1.6">
			jsver = 1.6;
			</script>
			<script  language="Javascript1.7">
			jsver = 1.7;
			</script>
			<script  language="Javascript1.8">
			jsver = 1.8;
			</script>
			<script  language="Javascript1.9">
			jsver = 1.9;
			</script>
			<script  language="Javascript2.0">
			jsver = 2.0;
			</script>
			<script type="text/javascript">
			document.write('Votre version de Javascript : Javascript ' + jsver
			);
			</script>
		<p>


		<p id="javascriptVersion">
			<script type="text/javascript">

			var nVer = navigator.appVersion;
			var nAgt = navigator.userAgent;
			var browserName  = navigator.appName;
			var fullVersion  = ''+parseFloat(navigator.appVersion); 
			var majorVersion = parseInt(navigator.appVersion,10);
			var nameOffset,verOffset,ix;

			// In Opera, the true version is after "Opera" or after "Version"
			if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
			browserName = "Opera";
			fullVersion = nAgt.substring(verOffset+6);
			if ((verOffset=nAgt.indexOf("Version"))!=-1) 
			fullVersion = nAgt.substring(verOffset+8);
			}
			// In MSIE, the true version is after "MSIE" in userAgent
			else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
			browserName = "Microsoft Internet Explorer";
			fullVersion = nAgt.substring(verOffset+5);
			}
			// In Chrome, the true version is after "Chrome" 
			else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
			browserName = "Chrome";
			fullVersion = nAgt.substring(verOffset+7);
			}
			// In Safari, the true version is after "Safari" or after "Version" 
			else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
			browserName = "Safari";
			fullVersion = nAgt.substring(verOffset+7);
			if ((verOffset=nAgt.indexOf("Version"))!=-1) 
			fullVersion = nAgt.substring(verOffset+8);
			}
			// In Firefox, the true version is after "Firefox" 
			else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
			browserName = "Firefox";
			fullVersion = nAgt.substring(verOffset+8);
			}
			// In most other browsers, "name/version" is at the end of userAgent 
			else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/')) ) 
			{
				browserName = nAgt.substring(nameOffset,verOffset);
				fullVersion = nAgt.substring(verOffset+1);
				if (browserName.toLowerCase()==browserName.toUpperCase()) {
					browserName = navigator.appName;
				}
			}
			// trim the fullVersion string at semicolon/space if present
			if ((ix=fullVersion.indexOf(";"))!=-1)
			fullVersion=fullVersion.substring(0,ix);
			if ((ix=fullVersion.indexOf(" "))!=-1)
			fullVersion=fullVersion.substring(0,ix);

			majorVersion = parseInt(''+fullVersion,10);
			if (isNaN(majorVersion)) {
			fullVersion  = ''+parseFloat(navigator.appVersion); 
			majorVersion = parseInt(navigator.appVersion,10);
			}

			document.write(''
			+'Browser name  = '+browserName+'<br>'
			+'Full version  = '+fullVersion+'<br>'
			+'Major version = '+majorVersion+'<br>'
			+'navigator.appName = '+navigator.appName+'<br>'
			+'navigator.userAgent = '+navigator.userAgent+'<br>'
			)
			</script>
		<p>

	</body>
</html>

