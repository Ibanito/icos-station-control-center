/**
 * @class ModelStationConfiguration
 *
 * Model class of the MVC pattern.
 * Manages the data of the application.
 */
class ModelStationConfiguration {

    /**
     * Create the MVC Model instance
     * 
     * @constructor
     */
    constructor() {

        this.tankList = []
        this.instrumentList = []
        this.lineList = []
        this.loadTankList()
        this.loadinstrumentList()
        this.loadlineList()

        //this.instrumentList = ['Picarro_1','Picarro_2','Picarro_3','Picarro_4','Picarro_5','Picarro_6','Picarro_7','Picarro_8','Picarro_9']
        //this.tankList = ['tank_1','tank_2','tank_3','tank_4','tank_5','tank_6','tank_7','tank_8','tank_9','tank_10','tank_11','tank_12','tank_13','tank_14','tank_15']
        //this.lineList = ['Line_1A','Line_2B','Line_3C','Line_4D','Line_5E','Line_6F']

        this.tankType = ['Reference tank', 'Calibration tank', 'Intercomparaisont tank','Short term target tank','Long term target tank','Test tank','Measure tank']


        this.stationList = [
            {name : 'Biscarrosse', 
                sequenceur : [{id :"1", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"2", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"3", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4}], 
                timeZone : "", 
                selected : false},
            {name : 'Kourou', 
                sequenceur : [{id :"a", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"b", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"c", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4}], 
                timeZone : "",
                selected : false},
            {name : 'Pic du Midi', 
                sequenceur : [{id :"toto", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"titi", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"tutu", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4}], 
                timeZone : "", 
                selected : false},
            {name : 'Réunion', 
                sequenceur : [{id :"un", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"deux", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4},
                    {id :"trois", selected : false, instruments: [], rows: [], valcoNum: 16, solenoidNum: 4}], 
                timeZone : "", 
                selected : false}
        ]

        this.stationList = JSON.parse(localStorage.getItem('stationConfig')) || this.stationList
        //this.resetSelected()

        this.instruments = []
        this.valcoNum = 16
        this.solenoidNum = 4
        this.rows = []

        this.loadSelected()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * put the "selected" attributes to false for all sequenceur for all station in this.stationList
     * @private
     */
    resetSelected(){
        this.stationList.forEach(station =>{
            station.sequenceur.forEach(seq =>{
                seq.selected = false
            })
        })
    }

    /**
     * put the of the selected station sequenceur into this.instruments, this.rows, this.valcoNum and this.solenoidNum
     * @private
     */
    loadSelected(){
        this.stationList.forEach(station =>{
            if(station.selected){
                station.sequenceur.forEach(seq =>{
                    if(seq.selected){
                        this.instruments = seq.instruments
                        this.valcoNum = seq.valcoNum
                        this.solenoidNum = seq.solenoidNum
                        this.rows = seq.rows
                    }
                })
            }
        })
    }

    /**
     * charge le texte du fichier de l'url dans un tableau puis appelle le callback avec ce tableau en parametre
     * @param {string} url 
     * @param {} callback 
     */
    getList(url,callback){
        // read text from URL location
        let request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.send(null);
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                let type = request.getResponseHeader('Content-Type');
                if (type.indexOf("text") !== 1) {
                    let content = request.responseText;
                    content = content.split('\n')
                    callback(content)
                }
            }
        }
    }
    
    /**
     * charge la liste de tank depuis un fichier csv de l'url
     */
    loadTankList(){
        let url  = 'http://localhost/icos-station-control-center/html/Assets_Documents/Tanks_available_for_the_stations.csv'
        this.getList(url, (content)=>{
            this.tankList = content
            this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
        })
    }

    /**
     * charge la liste d'instrument depuis un fichier csv de l'url
     */
    loadinstrumentList(){
        let url  = 'http://localhost/icos-station-control-center/html/Assets_Documents/Instruments_of_the_station.csv'
        this.getList(url, (content)=>{
            this.instrumentList = content
            this.onInstrumentsChanged(this.instruments,this.instrumentList)
        })
    }

    /**
     * charge la liste de ligne depuis un fichier csv de l'url
     */
    loadlineList(){
        let url  = 'http://localhost/icos-station-control-center/html/Assets_Documents/Lines_of_the_station.csv'
        this.getList(url, (content)=>{
            this.lineList = content
            this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
        })
    }

    //////////////////////////////// SAVE ///////////////////////////////////////

    /**
     * add the data from this.instruments, this.rows, this.valcoNum and this.solenoidNum in this.stationList
     * then create a JSON from this.stationList and save it localStorage 
     */
    saveData(){
        //console.log('save beginning')
        let selectedStation = ""
        let indiceStation = 0
        let selectedSequenceur = ""
        let indiceSeq = 0
        let compt = 0

        this.stationList.forEach(station =>{
            if(station.selected){
                selectedStation = station.name
                indiceStation = compt
            }
            else {
                compt=compt+1
            }
        })
        compt = 0
        if(selectedStation !== ""){
            this.stationList[indiceStation].sequenceur.forEach(seq =>{
                if(seq.selected){
                    selectedSequenceur = seq.id
                    indiceSeq = compt
                } else {
                    compt=compt+1
                }
            })
            if(selectedSequenceur !== ""){
                this.stationList[indiceStation].sequenceur[indiceSeq].instruments = this.instruments 
                this.stationList[indiceStation].sequenceur[indiceSeq].rows = this.rows
                this.stationList[indiceStation].sequenceur[indiceSeq].valcoNum = this.valcoNum
                this.stationList[indiceStation].sequenceur[indiceSeq].solenoidNum = this.solenoidNum
            }
        }
        localStorage.setItem('stationConfig', JSON.stringify(this.stationList))

        console.log('save finished')
    }


    /////////////////////////////////// STATION INFO //////////////////////////////////////////////

    /**
     * change the selected station in this.stationList
     * 
     * @param {string} name 
     */
    selectStationName(name){
        this.stationList = this.stationList.map( station =>
            station.name === name ? {name : station.name, sequenceur : station.sequenceur, timeZone : station.timeZone, selected : true} : {name : station.name, sequenceur : station.sequenceur, timeZone : station.timeZone, selected : false}
        )
        //this.onStationOptionChanged(this.stationList)
        this.selectSequenceurNum(name, "")
    }

    /**
     * Change the selected sequenceur of a station and load the data of this sequenceur from this.stationList to this.rows, this.instruments, this.valcoNum and this.solenoidNum
     * if seqId is "" then none of the sequenceur is selected
     * @param {string} stationName 
     * @param {string} SeqId 
     */
    selectSequenceurNum(stationName, SeqId){
        if(SeqId === ""){
            this.rows = []
            this.instruments = []
            this.valcoNum = 16
            this.solenoidNum = 4
        }
        this.stationList.forEach(station => {
            if(station.name === stationName){
                station.sequenceur.forEach(seq =>{
                    if(seq.id === SeqId){
                        seq.selected = true
                        this.rows = seq.rows
                        this.instruments = seq.instruments
                        this.valcoNum = seq.valcoNum
                        this.solenoidNum = seq.solenoidNum
                    }
                    else{
                        seq.selected = false
                    }
                })
            }
        })
        this.onValcoSolenoidNumChanged(this.valcoNum,this.solenoidNum)
        this.onStationOptionChanged(this.stationList)
        this.onInstrumentsChanged(this.instruments,this.instrumentList)
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * Change the timeZone value of the selected station
     * @param {string} name 
     * @param {string} time 
     */
    setTimeZone(name,time){
        this.stationList = this.stationList.map( station =>
            station.name === name ? {name : station.name, sequenceur : station.sequenceur, timeZone : time, selected : true} : station
        )
        this.onStationOptionChanged(this.stationList)
    }
    

    ////////////////////////////////////////// INSTRUMENT INFO ///////////////////////////////////////////

    /**
     * add a unnammed instrument to the list this.instrument
     */
    addInstrument(){
        let newid = this.instruments.length > 0 ? this.instruments[this.instruments.length - 1].id + 1 : 1
        this.instruments.push({name : "", id : newid})
        //console.log("instrument "+ newid +" added")
        this.onInstrumentsChanged(this.instruments,this.instrumentList)
    }

    /**
     * delete the corresponding instrument in this.instruments
     * @param {int} id 
     */
    deleteInstrument(id){
        this.instruments = this.instruments.filter(instrument => instrument.id !== id)
        //console.log("instru "+ id + " delete")
        this.onInstrumentsChanged(this.instruments,this.instrumentList)
    }

    /**
     * change the name to instrumentName of the corresponding instrument in this.instruments
     * @param {int} id 
     * @param {string} instrumentName 
     */
    changeInstrument(id,instrumentName){
        this.instruments = this.instruments.map(instrument =>
            instrument.id === id ? {name : instrumentName, id: instrument.id } : instrument
        )
        //console.log("instrument "+ instrumentName +" selected")
        this.onInstrumentsChanged(this.instruments,this.instrumentList)
    }

    ////////////////////////////////// TABLEAU ///////////////////////////////////////////////////////////

    /**
     * add a empty line in the table (in this.rows)
     */
    addLine(){
        let newid = this.rows.length > 0 ? this.rows[this.rows.length - 1].id + 1 : 1
        this.rows.push({id : newid, tag: "", valco: 0, solenoid : [], lineType: true, lineComposition: ["???"],databaseNum : "???"})
        //console.log("line added")
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * delete the corresponding line in this.rows
     * @param {int} id 
     */
    deleteLine(id){
        this.rows = this.rows.filter(row => row.id !== id)
        //console.log("line "+ id + " delete")
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)

    }

    /**
     * set the tag value of the corresponding line in rows to the new tag value
     * @param {int} id 
     * @param {string} tag 
     */
    setTag(id,tag){
        this.rows = this.rows.map(row =>
            row.id === id ? {id : row.id, tag: tag, valco: row.valco, solenoid : row.solenoid, lineType: row.lineType, lineComposition: row.lineComposition, databaseNum : row.databaseNum} : row
        )
        //console.log("tag "+ id +" changed to "+tag)
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * set the valcoNum value of the corresponding line in rows to the new valcoNum value
     * @param {int} id 
     * @param {int} valcoNum 
     */
    setValco(id,valcoNum){
        this.rows = this.rows.map(row =>
            row.id === id ? {id : row.id, tag: row.tag, valco: valcoNum, solenoid : row.solenoid, lineType: row.lineType, lineComposition: row.lineComposition, databaseNum : row.databaseNum} : row
        )
        //console.log("valco "+ id +" changed to "+valcoNum)
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * set the value of the solenoid number solenoidNum to this opposite (true if false and false if true)
     * @param {int} id 
     * @param {int} solenoidNum 
     */
    setSolenoid(id,solenoidNum){
        this.rows.forEach(row =>{
            if(row.id === id){
                let newSolenoid = []
                let b = true
                row.solenoid.forEach(sol =>{
                    sol !== solenoidNum ? newSolenoid.push(sol) : b = false
                })
                if(b){
                    newSolenoid.push(solenoidNum)
                }
                //let newRow = {id : row.id, tag: row.tag, valco: row.valco, solenoid : newSolenoid, lineType: row.lineType, lineComposition: row.lineComposition, databaseNum : row.databaseNum}
                row.solenoid = newSolenoid
            }
        })
        //console.log(this.rows)
        //console.log("solenoid "+ solenoidNum +" changed in row "+id)
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * reverse the lineType value (it is a bollean) in the corresponding line in this.rows
     * also reset the value of lineComposition in the corresponding line in this.rows
     * @param {int} id 
     */
    setLineType(id){
        this.rows.forEach(row =>{
            if(row.id === id){
                row.lineType = !row.lineType
                row.lineType ? row.lineComposition = ["???"] : row.lineComposition = ["???","???"]
            }
        })
        //console.log("LineType "+ id +" changed")
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * set the value of lineComposition of the corresponding line in this.rows to [LineName]
     * @param {int} id 
     * @param {string} LineName 
     */
    setLine(id,LineName){
        this.rows = this.rows.map(row =>
            row.id === id ? {id : row.id, tag: row.tag, valco: row.valco, solenoid : row.solenoid, lineType: row.lineType, lineComposition: [LineName], databaseNum : row.databaseNum} : row
        )
        //console.log("setLine")
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * set the value of lineComposition[0] of the corresponding line in this.rows to tankTypeName
     * @param {int} id 
     * @param {string} tankTypeName 
     */
    setTankType(id,tankTypeName){
        this.rows = this.rows.map(row =>
            row.id === id ? {id : row.id, tag: row.tag, valco: row.valco, solenoid : row.solenoid, lineType: row.lineType, lineComposition: [tankTypeName, row.lineComposition[1]], databaseNum : row.databaseNum} : row
        )
        //console.log("setTankType")
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }

    /**
     * set the value of lineComposition[1] of the corresponding line in this.rows to TankName
     * @param {int} id 
     * @param {string} TankName 
     */
    setTank(id,TankName){
        this.rows = this.rows.map(row =>
            row.id === id ? {id : row.id, tag: row.tag, valco: row.valco, solenoid : row.solenoid, lineType: row.lineType, lineComposition: [row.lineComposition[0], TankName], databaseNum : row.databaseNum} : row
        )
       //console.log("setTank")
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * set tihs.valcoNum to num
     * @param {int} num 
     */
    setValcoNum(num){
        this.valcoNum = num
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
        this.onValcoSolenoidNumChanged(this.valcoNum,this.solenoidNum)
    }

    /**
     * set tihs.solenoidNum to num
     * @param {int} num 
     */
    setSolenoidNum(num){
        this.solenoidNum = num
        this.onTableauChanged(this.rows,this.tankType,this.tankList,this.lineList,this.valcoNum,this.solenoidNum)
        this.onValcoSolenoidNumChanged(this.valcoNum,this.solenoidNum)
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    /**
     * set the station shecmatics image to the new one
     * @param {file[]} files 
     */
    setImage(files){
        console.log(files[0].name)

        let file = files[0]
        let reader = new FileReader()

        let self = this
        reader.onload = function(event) {
            let imgData = event.target.result;
            localStorage.setItem('StationSchematic',imgData)
            self.onImageChanged()
        }
        
        reader.readAsDataURL(file);    
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * When the model stationList list has changed, the changes has to be seen on the View.
     * This function links the view with the model to upadte the stationList drawing.
     * 
     * @param callback
     */
    bindStationOptionChanged(callback) {
        this.onStationOptionChanged = callback
    }

    /**
     * When the model instruments list has changed, the changes has to be seen on the View.
     * This function links the view with the model to upadte the instruments drawing.
     * 
     * @param callback
     */
    bindInstrumentsChanged(callback){
        this.onInstrumentsChanged = callback
    }

    /**
     * When the model rows list has changed, the changes has to be seen on the View.
     * This function links the view with the model to upadte the table drawing.
     * 
     * @param callback
     */
    bindTableauChanged(callback) {
        this.onTableauChanged = callback
    }

    /**
     * When the model valcoNum and solenoidNum  has changed, the changes has to be seen on the View.
     * This function links the view with the model to upadte the drawing.
     * 
     * @param callback
     */
    bindValcoSolenoidNumChanged(callback) {
        this.onValcoSolenoidNumChanged = callback
    }

    /**
     * When the stored image  has changed, the changes has to be seen on the View.
     * This function links the view with the stored image to upadte the drawing.
     * 
     * @param callback
     */
    bindImageChanged(callback){
        this.onImageChanged = callback
    }


}



