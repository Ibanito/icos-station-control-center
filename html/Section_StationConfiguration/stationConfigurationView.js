/**
 * @class ViewStationConfiguration
 * 
 * View class of the MVC pattern.
 * 
 */
class ViewStationConfiguration {

    /**
     * Create the MVC View instance
     * The main DOM squeletton is created here.
     * 
     * @constructor
     */
    constructor() {
        this.topJsContainer = this.getElement("#topJsContainer")
        this.tableContainer = this.getElement("#tableContainer")

        //STATION INFO DIV
        this.stationInfo = this.createElement('div', 'stationInfo')

        this.stationInfoTitle = this.createElement('h2', 'stationInfoTitle')
        this.stationInfoTitle.textContent = 'ICOS Station data'

        this.stationNameSelectLabel = this.createElement('label')
        this.stationNameSelectLabel.for = '#stationNameSelect'
        this.stationNameSelectLabel.textContent = 'Station :'
        this.stationNameSelect = this.createElement('select', 'stationSelect')

        this.sequenceurNumSelectLabel = this.createElement('label')
        this.sequenceurNumSelectLabel.for = 'sequenceurNumSelect'
        this.sequenceurNumSelectLabel.textContent = 'Sequenceur :'
        this.sequenceurNumSelect = this.createElement('select', 'stationSelect')

        this.timeZoneSelectLabel = this.createElement('label')
        this.timeZoneSelectLabel.for = '#timeZoneSelect'
        this.timeZoneSelectLabel.textContent = 'Time zone :'
        this.timeZoneSelect = this.createElement('select', 'stationSelect')

        this.stationInfo.append(this.stationInfoTitle,this.stationNameSelectLabel, this.stationNameSelect)
        this.stationInfo.append(this.sequenceurNumSelectLabel, this.sequenceurNumSelect)
        this.stationInfo.append(this.timeZoneSelectLabel, this.timeZoneSelect)

        this.topJsContainer.append(this.stationInfo)
        // FIN STATION INFO DIV

        // INSTRUMENT INFO DIV
        this.instrumentInfo = this.createElement('div', 'instrumentInfo')

        this.instrumentInfoTitle = this.createElement('h2', 'instrumentInfoTitle')
        this.instrumentInfoTitle.textContent = 'Instruments'

        this.instrumentInfoList = this.createElement('ul', 'instrumentInfoList')

        this.instrumentInfo.append(this.instrumentInfoTitle,this.instrumentInfoList)
        this.topJsContainer.append(this.instrumentInfo)
        // FIN INSTRUMENT INFO DIV

        // BIG BUTTON DIV
        this.bigButtonContainer = this.createElement('div', 'bigButtonContainer')

        this.updateDataButton = this.createElement('button', 'updateDataButton')
        this.updateDataButton.textContent = 'Update the data'

        this.saveChangesButton = this.createElement('button', 'saveChangesButton')
        this.saveChangesButton.textContent = 'Save changes'

        this.bigButtonContainer.append(this.updateDataButton, this.saveChangesButton)


        this.topJsContainer.append(this.bigButtonContainer)
        // FIN BIG BUTTON DIV

        // TABLEAU DIV
        this.table = this.createElement('table','table')
        this.tableTitle = this.createElement('thead', 'columTile')
        this.headLine = this.createElement('tr','captionTr')
        this.theadCreator()
        this.tbody = this.createElement('tbody','tabbody')
        this.tfoot = this.createElement('tfoot','tabfoot') 
        this.addLineButton = this.createElement('button','tabAddLineButton')
        this.addLineButton.textContent = "Add a new line"
        //this.addLineButton.colspan = '2'

        this.tableTitle.append(this.headLine)
        this.tfoot.append(this.addLineButton)
        this.table.append(this.tableTitle,this.tbody,this.tfoot)
        this.tableContainer.append(this.table)
        // FIN TABLEAU DIV

        // VALCO VALVE ET SOLENOID NUMBER 
        this.numDiv = this.createElement('div', 'numDiv')
        this.valcoNumDiv = this.createElement('div','valcoNumDiv')
        this.valcoNumSelect = this.createElement('select','valcoNumSelect')

        this.valcoNumLabel = this.createElement('label')
        this.valcoNumLabel.for = this.valcoNumSelect
        this.valcoNumLabel.textContent = "Number of Valco \r\n valve positions :"
        this.valcoNumDiv.append(this.valcoNumLabel,this.valcoNumSelect)
        this.numDiv.append(this.valcoNumDiv)
        this.tableContainer.append(this.numDiv)

        this.solenoidNumDiv = this.createElement('div','solenoidNumDiv')
        this.solenoidNumSelect = this.createElement('select','solenoidNumSelect')
        
        this.solenoidNumLabel = this.createElement('label')
        this.solenoidNumLabel.for = this.solenoidNumSelect
        this.solenoidNumLabel.textContent = "Number of solenoid valves :"
        this.solenoidNumDiv.append(this.solenoidNumLabel,this.solenoidNumSelect)
        this.numDiv.append(this.solenoidNumDiv)
        this.tableContainer.append(this.numDiv)
        // VALCO VALVE ET SOLENOID NUMBER FIN

        this.uploadIntput = this.createElement('input','uploadIntput')
        this.uploadIntput.type = "file"
        this.uploadIntput.textContent = "upload image"
        this.uploadIntput.accept = "image/png, image/jpeg"

        this.uploadIntputLabel = this.createElement('label')
        this.uploadIntputLabel.textContent = "Station schematic:"
        this.uploadIntput.for = this.uploadIntput

        this.stationImageDiv = this.createElement('div', 'stationImageDiv')
        this.stationImageDiv.append(this.uploadIntputLabel,this.uploadIntput)
        this.tableContainer.append(this.stationImageDiv)
    }
    /////////////////////////////////////// STATION INFO /////////////////////////////////////////////////////////////////////////////////
    /**
     * display the station, sequenceur and timeZone selector in the top left
     * @param {object} stationList 
     */
    displayStationInfoOption (stationList){
        //delete all options in the selects of the stationInfo div
        while(this.stationNameSelect.firstChild){
            this.stationNameSelect.removeChild(this.stationNameSelect.firstChild)
        }
        while(this.sequenceurNumSelect.firstChild){
            this.sequenceurNumSelect.removeChild(this.sequenceurNumSelect.firstChild)
        }
        while(this.timeZoneSelect.firstChild){
            this.timeZoneSelect.removeChild(this.timeZoneSelect.firstChild)
        }
        // create a default selected value
        let baseValue = this.createElement('option')
        baseValue.value = ""
        baseValue.textContent = "--select a station--"
        baseValue.selected = true
        this.stationNameSelect.append(baseValue)
        //message if stationList is empty
        if(stationList.length === 0){
            let mes = this.createElement('option')
            mes.textContent = "no station available"
            this.stationNameSelect.append(mes)
        }

        else{
            //on ajoute les noms des station dans les options de stationNameSelect
            stationList.forEach (station => {
                let st = this.createElement('option')
                st.value = station.name
                st.textContent = station.name
                if(station.selected){
                    st.selected = true
                    let timeZone = this.createElement('option')
                    timeZone.textContent = station.timeZone
                    timeZone.value = station.timeZone
                    timeZone.selected = true
                    this.timeZoneSelect.append(timeZone)
                }
                this.stationNameSelect.append(st)
            })
            //on ajoute les id des sequenceurs dans les option de sequenceurNumSelect
            stationList.forEach(station => {
                if(station.selected){
                    let defaultSeq = this.createElement('option')
                    defaultSeq.value = ""
                    defaultSeq.textContent = "--select a sequenceur--"
                    defaultSeq.selected = true
                    this.sequenceurNumSelect.append(defaultSeq)
                    station.sequenceur.forEach(sequenceur => {
                        let seq = this.createElement('option')
                        seq.value = sequenceur.id
                        seq.textContent = sequenceur.id
                        if(sequenceur.selected){
                            seq.selected = true
                        }
                        this.sequenceurNumSelect.append(seq)
                    })
                }
            })
            // on ajoute les 24 options dans timeZoneSelect
            for(let i=-12;i<=12;i++){
                let timeZone = this.createElement('option')
                timeZone.textContent = i<0 ? ""+i : "+"+i
                timeZone.value =  i<0 ? ""+i : "+"+i
                this.timeZoneSelect.append(timeZone)
            }
        }
        //console.log("displayStationInfoOption done")
    }

    ///////////////////////////////////////////////////////// INSTRUMENT INFO /////////////////////////////////////////////////////////////////////////////////////////
    /**
     * display the instruments of the selected station/sequenceur in the top center
     * @param {object} instruments 
     * @param {array[string]} instrumentList 
     */
    displayInstrumentInfo(instruments,instrumentList){
        // delete the old display
        while(this.instrumentInfoList.firstChild){
            this.instrumentInfoList.removeChild(this.instrumentInfoList.firstChild)
        }

        instruments.forEach(instrument =>{
            let instrBox = this.createElement('li', 'instrumentBox')
            instrBox.id = instrument.id

            let instrSelect = this.createElement('select', 'instrumentSelect')
            let instrDeleteButton = this.createElement('button', 'instrumentDeleteButton')
            instrDeleteButton.textContent = "Delete"
            // default option
            let defaultOption = this.createElement('option', 'instrOption')
            defaultOption.value = ""
            defaultOption.textContent = '-- no instrument selected --'
            defaultOption.selected = true
            instrSelect.append(defaultOption)

            instrumentList.forEach(instrFromList =>{
                let instrOption = this.createElement('option', 'instrOption')
                instrOption.value = instrFromList
                instrOption.textContent = instrFromList
                if(instrument.name === instrFromList){
                    instrOption.selected = true
                }
                instrSelect.append(instrOption)
            })
            this.instrumentInfoList.append(instrBox)
            instrBox.append(instrSelect,instrDeleteButton)
        })
        //ajoute le boutton pour ajouter un nouveau instrument
        this.addInstrument = this.createElement('li', 'addInstrument')
        this.instrumentInfoList.append(this.addInstrument)

        this.addInstrumentButton = this.createElement('button','addInstrumentButton')
        this.addInstrumentButton.textContent = 'Add a new instrument'
        this.addInstrument.append(this.addInstrumentButton)

        //console.log("cocococo")
    }

    ////////////////////////////////////// TABLEAU ////////////////////////////////////////////////////////////////////

    /**
     * created the title of each colum of the table for the construtor
     * @see constructor
     */
    theadCreator(){
        //this.captionLine = this.createElement('tr','captionTr')
        let firstC = this.createElement('th','headElement')
        firstC.scope = 'col'
        let secondC = this.createElement('th','headElement')
        secondC.textContent = "Input line connected \r\n to the instruments"
        secondC.scope = 'col'
        let thirdC = this.createElement('th','headElement')
        thirdC.textContent = "VALCO"
        thirdC.scope = 'col'
        let fourthC = this.createElement('th','headElement')
        fourthC.textContent = "Solenoid Valves"
        fourthC.scope = 'col'
        let fifthC = this.createElement('th','headElement')
        fifthC.textContent = "Line type"
        fifthC.scope = 'col'
        let sixthC = this.createElement('th','headElement')
        sixthC.textContent = "Line composition"
        sixthC.scope = 'col'
        let seventhC = this.createElement('th','headElement')
        seventhC.textContent = "Combination \r\n representation \r\n in the database"
        seventhC.scope = 'col'
        this.headLine.append(firstC,secondC,thirdC,fourthC,fifthC,sixthC,seventhC)
    }
    
    /**
     * display the table
     * @param {object} rows the rows of the table
     * @param {array[string]} tankType list of the different tankType
     * @param {array[string]} tankList list of the available tanks
     * @param {array[string]} lineList list of the available lines
     * @param {int} valcoNum 
     * @param {int} solenoidNum 
     */
    displayTableau(rows,tankType,tankList,lineList,valcoNum,solenoidNum){
        while(this.tbody.firstChild){
            this.tbody.removeChild(this.tbody.firstChild)
        }
        // for each row of the table
        rows.forEach(row =>{
            let line = this.createElement('tr','tabLine')
            line.id = row.id
            //add the delete button
            let firstC = this.createElement('td','lineElement') 
            let deleteButton = this.createElement('button','lineDeleteButton')
            deleteButton.textContent = 'Delete'
            firstC.append(deleteButton)
            // add the tag input
            let secondC = this.createElement('td','lineElement')
            let tagForm = this.createElement('input', 'tagForm')
            tagForm.value = row.tag
            secondC.append(tagForm)
            // add a selector for the valco
            let thirdC = this.createElement('td','lineElement')
            let valcoSelect = this.createElement('select','valcoSelect')
            for(let i=1;i<=valcoNum;i++){
                let valocOption = this.createElement('option','valocOption')
                valocOption.value = i
                valocOption.textContent = i
                if(row.valco == i){
                    valocOption.selected = true
                }
                valcoSelect.append(valocOption)
            }
            thirdC.append(valcoSelect)
            // add a checkbox for each solenoid valve available
            let fourthC = this.createElement('td','lineElement')
            for(let i=1;i<=solenoidNum;i++){
                let solenoidCheck = this.createElement('input','solenoidCheck')
                solenoidCheck.type = "checkbox"
                solenoidCheck.id = i
                row.solenoid.forEach(v=>{
                    v==i ? solenoidCheck.checked = true : {}
                })
                let checkLabel = this.createElement('label','checkLabel')
                checkLabel.for = i
                checkLabel.textContent = i
                fourthC.append(solenoidCheck,checkLabel)
            }
            // add two radio button to select the line type
            let fifthC = this.createElement('td','lineElement')
            let LineTypeTank = this.createElement('input','lineType')
            LineTypeTank.type = "radio"
            LineTypeTank.id = "tank"
            let labelTank = this.createElement('label', 'checkLabel')
            labelTank.for = "tank"
            labelTank.textContent = "tank"
            let LineTypeAmbient = this.createElement('input','lineType')
            LineTypeAmbient.type = "radio"
            LineTypeAmbient.id = "ambient"
            let labelAmbient = this.createElement('label', 'checkLabel')
            labelAmbient.for = "ambient"
            labelAmbient.textContent = "ambient"
            if(row.lineType)  {
                LineTypeAmbient.checked = true
                LineTypeTank.checked = false
            } else {
                LineTypeTank.checked = true
                LineTypeAmbient.checked = false
            } 
            fifthC.append(LineTypeTank,labelTank,LineTypeAmbient,labelAmbient)
            // add the line composition selectors
            let sixthC = this.createElement('td','lineCompositionElement')
            if(row.lineType){ // si le lineType est vrai (ambient)
                let lineSelect = this.createElement('select', 'lineSelect')
                if(row.lineComposition === "???"){
                    let defaultOption = this.createElement('option','lineOption')
                    defaultOption.value = ""
                    defaultOption.textContent = "???"
                    defaultOption.selected = true
                    lineSelect.append(defaultOption)
                }
                lineList.forEach(line =>{
                    let lineOption = this.createElement('option','lineOption')
                    lineOption.value = line
                    lineOption.textContent = line
                    if(row.lineComposition[0] === line){
                        lineOption.selected = true
                    }
                    lineSelect.append(lineOption)
                })
                sixthC.append(lineSelect)
            }
            else{ // si le lineType est tank
                let tankTypeSelect = this.createElement('select', 'tankTypeSelect')
                let tankSelect = this.createElement('select', 'tankSelect')
                if(row.lineComposition[0] === "???"){
                    let defaultOption = this.createElement('option','tankTypeOption')
                    defaultOption.value = ""
                    defaultOption.textContent = "???"
                    defaultOption.selected = true
                    tankTypeSelect.append(defaultOption)
                }
                if(row.lineComposition[1] === "???"){
                    let defaultOption = this.createElement('option','tankOption')
                    defaultOption.value = ""
                    defaultOption.textContent = "???"
                    defaultOption.selected = true
                    tankSelect.append(defaultOption)
                }
                tankType.forEach(tankt =>{
                    let tankTypeOption = this.createElement('option','tankTypeOption')
                    tankTypeOption.value = tankt
                    tankTypeOption.textContent = tankt
                    if(row.lineComposition[0] === tankt){
                        tankTypeOption.selected = true
                    }
                    tankTypeSelect.append(tankTypeOption)
                })
                let fleche = this.createElement('p','fleche')
                fleche.textContent = '➔'
                sixthC.append(tankTypeSelect,fleche)
                tankList.forEach(tank =>{
                    let tankOption = this.createElement('option','tankOption')
                    tankOption.value = tank
                    tankOption.textContent = tank
                    if(row.lineComposition[1] === tank){
                        tankOption.selected = true
                    }
                    tankSelect.append(tankOption)
                })
                sixthC.append(tankSelect)
            }
            
            // add the display for the dataBaseNumber
            let seventhC = this.createElement('td','lineElement')
            seventhC.textContent = row.databaseNum

            line.append(firstC,secondC,thirdC,fourthC,fifthC,sixthC,seventhC)
            this.tbody.append(line)
        })

    }

    /**
     * display the selectors for the number of valco valve and solenoid valve in the station sequenceur
     * @param {int} valcoNum 
     * @param {int} solenoidNum 
     */
    displayValcoSolenoidNum(valcoNum,solenoidNum){
        for(let i=1; i<=18; i++){
            let valcoNumOption = this.createElement('option','valcoNumOption')
            valcoNumOption.value = i
            valcoNumOption.textContent = i
            if(i == valcoNum){
                valcoNumOption.selected = true
            }
            this.valcoNumSelect.append(valcoNumOption)
        }
        for(let i=1; i<=63; i++){
            let solenoidNumOption = this.createElement('option','solenoidoNumOption')
            solenoidNumOption.value = i
            solenoidNumOption.textContent = i
            if(i == solenoidNum){
                solenoidNumOption.selected = true
            }
            this.solenoidNumSelect.append(solenoidNumOption)
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    displayStationImage(){
        if(this.stationImageDiv.lastChild.className === 'stationImage'){
            this.stationImageDiv.lastChild.remove()
        }

        let imageFile = localStorage.getItem('StationSchematic');
        let img = this.createElement('img','stationImage')
        img.src = imageFile
        this.stationImageDiv.append(img)
    }

    ////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add an element to the DOM and return the created DOM element with a optional css class name
     *
     * @private
     * @param {string} tag
     * @param {string} className
     * @return {element} element
     */
    createElement(tag, className) {
        const element = document.createElement(tag)
        if (className) element.classList.add(className)

        return element
    }

    /**
     * Get an element from the DOM
     *
     * @private
     * @param {string} selector
     * @return {element} element
     */
    getElement(selector) {
        const element = document.querySelector(selector)

        return element
    }


    //////////////////////////////////////////////// STATION INFO /////////////////////////////////////////////////////////////////////////

    /**
     * This function links the "station" selector to the MVC Controller.
     * @see model.selectStationName
     * @private
     * @param {} handler
     */
    bindselectStationName(handler) {
        this.stationNameSelect.addEventListener("change", event => {
            //console.log(this.stationNameSelect.value)
            handler(this.stationNameSelect.value)
        })
    }

    /**
     * This function links the "sequenceur" selector to the MVC Controller.
     * @see model.selectSequenceurNum
     * @private
     * @param {} handler
     */
    bindselectSequenceurNum(handler) {
        this.sequenceurNumSelect.addEventListener("change", event => {
            //console.log(this.sequenceurNumSelect.value)
            handler(this.stationNameSelect.value,this.sequenceurNumSelect.value)
        })
    }

    /**
     * This function links the "time zone" selector to the MVC Controller.
     * @see model.setTimeZone
     * @private
     * @param {} handler
     */
    bindsetTimeZone(handler) {
        this.timeZoneSelect.addEventListener("change", event => {
            handler(this.stationNameSelect.value,this.timeZoneSelect.value)
        })
    }

    /////////////////////////////////////////////////// BIG BUTTON CONTAINER //////////////////////////////////////////////////////////////////////
    /**
     * This function links the "save changes" button to the MVC Controller.
     * @see model.saveData
     * @private
     * @param {} handler
     */
    bindsaveData(handler){
        this.saveChangesButton.addEventListener("click", event =>{
            handler()
        })
    }    

    ///////////////////////////////////////////////////////  INSTRUMENT INFO //////////////////////////////////////////////////////////////////    
    /**
     * This function links the "add instrument" button to the MVC Controller.
     * @see model.addInstrument
     * @private
     * @param {} handler
     */
    bindaddInstrument(handler) {
        this.instrumentInfoList.addEventListener("click", event => {
            if(event.target.className === "addInstrumentButton"){
                //console.log("button add instrument clicked")
                handler()
            }

        })

    }

    /**
     * This function links the "delete instrument" buttons to the MVC Controller.
     * @see model.deleteInstrument
     * @private
     * @param {} handler
     */
    binddeleteInstrument(handler) {
        this.instrumentInfoList.addEventListener("click", event =>{
            if(event.target.className === "instrumentDeleteButton"){
                const id = parseInt(event.target.parentElement.id)
                //console.log("id: " + event.target.parentElement.id)
                handler(id)
            }
        })
    }

    /**
     * This function links the "instrument name" selectors to the MVC Controller.
     * @see model.changeInstrument
     * @private
     * @param {} handler
     */
    bindchangeInstrument(handler) {
        this.instrumentInfoList.addEventListener("change", event => {
            if(event.target.className === "instrumentSelect"){
                const id = parseInt(event.target.parentElement.id)
                handler(id,event.target.value)
            }
        })
    }

    ////////////////////////////////////////////////////// TABLE /////////////////////////////////////////////////////////////////////////
    /**
     * This function links the "add a new line" button to the MVC Controller.
     * @see model.addLine
     * @private
     * @param {} handler
     */
    bindaddLine(handler){
        this.addLineButton.addEventListener('click',event =>{
            handler()
        })
    }

    /**
     * This function links the "delete" line buttons to the MVC Controller.
     * @see model.deleteLine
     * @private
     * @param {} handler
     */
    binddeleteLine(handler){
        this.tbody.addEventListener('click',event =>{
            if(event.target.className === "lineDeleteButton"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id)
            }
        })
    }

    /**
     * This function links the table tag input to the MVC Controller.
     * @see model.setTag
     * @private
     * @param {} handler
     */
    bindsetTag(handler){
        this.tbody.addEventListener('change', event =>{
            if(event.target.className === "tagForm"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id,event.target.value)
            }
        })
    }

    /**
     * This function links the "valco" selectors to the MVC Controller.
     * @see model.setValco
     * @private
     * @param {} handler
     */
    bindsetValco(handler){
        this.tbody.addEventListener('change', event =>{
            if(event.target.className === "valcoSelect"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id,event.target.value)
            }
        })

    }

    /**
     * This function links the "solenoid" checkboxes to the MVC Controller.
     * @see model.setSolenoid
     * @private
     * @param {} handler
     */
    bindsetSolenoid(handler){
        this.tbody.addEventListener('change', event=>{
            if(event.target.className === "solenoidCheck"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id,event.target.id)
            }
        })
    }

    /**
     * This function links the "Line type" radio buttons to the MVC Controller.
     * @see model.setLineType
     * @private
     * @param {} handler
     */
    bindsetLineType(handler){
        this.tbody.addEventListener('change', event=>{
            if(event.target.className === "lineType"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id)
            }
        })
    }

    /**
     * This function links the "line" selectors to the MVC Controller.
     * @see model.setLine
     * @private
     * @param {} handler
     */
    bindsetLine(handler){
        this.tbody.addEventListener('change', event=>{
            if(event.target.className === "lineSelect"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id, event.target.value)
            }
        })
    }

    /**
     * This function links the "tankType" selectors to the MVC Controller.
     * @see model.setTankType
     * @private
     * @param {} handler
     */
    bindsetTankType(handler){
        this.tbody.addEventListener('change', event=>{
            if(event.target.className === "tankTypeSelect"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id, event.target.value)
            }
        })
    }

    /**
     * This function links the "tanks" selectors to the MVC Controller.
     * @see model.setTank
     * @private
     * @param {} handler
     */
    bindsetTank(handler){
        this.tbody.addEventListener('change', event=>{
            if(event.target.className === "tankSelect"){
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id, event.target.value)
            }
        })
    }


    /////////////////////////////////// VALCO NUM AND SOLENOID NUM//////////////////////////////////////////////////////
    /**
     * This function links the "Number of Valco valve positions :" selectors to the MVC Controller.
     * @see model.setValcoNum
     * @private
     * @param {} handler
     */
    bindsetValcoNum(handler){
        this.valcoNumSelect.addEventListener('change', event=>{
            handler(event.target.value)
        })
    }
    /**
     * This function links the "Number of solenoid valves :" selectors to the MVC Controller.
     * @see model.setSolenoidNum
     * @private
     * @param {} handler
     */
    bindsetSolenoidNum(handler){
        this.solenoidNumSelect.addEventListener('change', event=>{
            handler(event.target.value)
        })
    }

    /**
     * This function links the upload image input to the MVC Controller.
     * @see model.setImage
     * @private
     * @param {} handler
     */
    bindsetImage(handler){
        this.uploadIntput.addEventListener('change', event =>{
            handler(event.target.files)
        })
    }



}