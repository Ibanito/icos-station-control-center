/**
 * @class ControllerStationConfiguration
 * 
 * Controller class of the MVC pattern
 * Links the user input and the view output.
 *
 */
class ControllerStationConfiguration {
    /**
     * Create the MVC Conttroller instance
     * All of the button view events are binded here.
     * 
     * @constructor
     * @param model
     * @param view
     */
    constructor(model, view) {
        this.model = model
        this.view = view


        this.view.bindselectStationName(this.handleSelectStationName)
        this.view.bindselectSequenceurNum(this.handleSelectSequenceurNum)
        this.view.bindaddInstrument(this.handleAddInstrument)
        this.view.binddeleteInstrument(this.handleDeleteInstrument)
        this.view.bindchangeInstrument(this.handleChangeInstrument)
        this.view.bindaddLine(this.handleAddLine)
        this.view.binddeleteLine(this.handleDeleteLine)
        this.view.bindsetTag(this.handleSetTag)
        this.view.bindsetValco(this.handleSetValco)
        this.view.bindsetSolenoid(this.handleSetSolenoid)
        this.view.bindsetLineType(this.handleSetLineType)
        this.view.bindsetLine(this.handleSetLine)
        this.view.bindsetTankType(this.handleSetTankType)
        this.view.bindsetTank(this.handleSetTank)
        this.view.bindsetValcoNum(this.handleSetValcoNum)
        this.view.bindsetSolenoidNum(this.handleSetSolenoidNum)
        this.view.bindsetTimeZone(this.handleSetTimeZone)
        this.view.bindsaveData(this.handleSaveData)
        this.view.bindsetImage(this.handleSetImage)

        this.model.bindStationOptionChanged(this.onStationOptionChanged)
        this.model.bindInstrumentsChanged(this.onInstrumentsChanged)
        this.model.bindTableauChanged(this.onTableauChanged)
        this.model.bindValcoSolenoidNumChanged(this.onValcoSolenoidNumChanged)
        this.model.bindImageChanged(this.onImageChanged)

        this.onValcoSolenoidNumChanged(this.model.valcoNum,this.model.solenoidNum)
        this.onStationOptionChanged(this.model.stationList)
        this.onInstrumentsChanged(this.model.instruments,this.model.instrumentList)
        this.onTableauChanged(this.model.rows,this.model.tankType,this.model.tankList,this.model.lineList,this.model.valcoNum,this.model.solenoidNum)
        this.onImageChanged()
    
    }

    /**
     * This function links the station name selector with the Model station name.
     * @see model.selectStationName
     */
    handleSelectStationName = name => {
        this.model.selectStationName(name)
    }
    /**
     * This function links the sequenceur selector with the Model selected sequenceur.
     * @see model.selectSequenceurNum
     */
    handleSelectSequenceurNum = (name, id) => {
        this.model.selectSequenceurNum(name,id)
    }
    /**
     * This function links the time zone selector with the Model time zone.
     * @see model.setTimeZone
     */
    handleSetTimeZone = (name, zone) => {
        this.model.setTimeZone(name,zone)
    }
    /**
     * This function links the save changes button selector with the Model.
     * @see model.saveData
     */
    handleSaveData = () => {
        this.model.saveData()
    }
    /**
     * This function links the add a new instrument button with the Model instruments.
     * @see model.addInstrument
     */
    handleAddInstrument = () => {
        this.model.addInstrument()
    }
    /**
     * This function links the delete instrumebts selector with the Model instruments.
     * @see model.deleteInstrument
     */
    handleDeleteInstrument = id => {
        this.model.deleteInstrument(id)
    }
    /**
     * This function links the instrument name selector with the Model instruments.
     * @see model.changeInstrument
     */
    handleChangeInstrument = (id,instrName) => {
        this.model.changeInstrument(id,instrName)
    }
    /**
     * This function links the add a new line button with the Model table data (rows).
     * @see model.addLine
     */
    handleAddLine = () =>{
        this.model.addLine()
    }
    /**
     * This function links the delete line button with the Model table data (rows).
     * @see model.deleteLine
     */
    handleDeleteLine = id =>{
        this.model.deleteLine(id)
    }
    /**
     * This function links the tag input with the Model table data (rows).
     * @see model.setTag
     */
    handleSetTag = (id,tag) =>{
        this.model.setTag(id,tag)
    }
    /**
     * This function links the valco selector with the Model table data (rows).
     * @see model.setValco
     */
    handleSetValco = (id,valcoNum) =>{
        this.model.setValco(id,valcoNum)
    }
    /**
     * This function links the solenoid chechkboxes with the Model table data (rows).
     * @see model.setSolenoid
     */
    handleSetSolenoid = (id, solenoidNum) =>{
        this.model.setSolenoid(id,solenoidNum)
    }
    /**
     * This function links the line type radio buttons with the Model table data (rows).
     * @see model.setLineType
     */
    handleSetLineType = id =>{
        this.model.setLineType(id)
    }
    /**
     * This function links the set line selector with the Model table data (rows).
     * @see model.setLine
     */
    handleSetLine = (id,name) =>{
        this.model.setLine(id,name)
    }
    /**
     * This function links the set tank type selector with the Model table data (rows).
     * @see model.setTankType
     */
    handleSetTankType = (id,name) =>{
        this.model.setTankType(id,name)
    }
    /**
     * This function links the set tank selector with the Model table data (rows).
     * @see model.setTank
     */
    handleSetTank = (id,name) =>{
        this.model.setTank(id,name)
    }
    /**
     * This function links the valco number selector with the Model valcoNum.
     * @see model.setValcoNum
     */
    handleSetValcoNum = num =>{
        this.model.setValcoNum(num)
    }
    /**
     * This function links the solenoid number selector with the Model solenoidNum.
     * @see model.setSolenoidNum
     */
    handleSetSolenoidNum = num =>{
        this.model.setSolenoidNum(num)
    }

    /**
     * This function change the file of the staion shematics image.
     * @see model.setImage
     */
    handleSetImage = files =>{
        this.model.setImage(files)
    }
    


    /**
     * This function links the Model Instruments changes in the storage with the View Instruments displaying.
     * @see view.displayInstrumentInfo
     */
    onInstrumentsChanged = (instruments,instrumentList) => {
        this.view.displayInstrumentInfo(instruments,instrumentList)
    }
    /**
     * This function links the stationLsit changes in the storage with the station info displaying.
     * @see view.displayStationInfoOption
     */
    onStationOptionChanged = stationList => {
        this.view.displayStationInfoOption(stationList)
    }
    /**
     * This function links the rows changes in the storage with the table displaying.
     * @see view.displayTableau
     */
    onTableauChanged = (rows,tankType,tankList,lineList,valcoNum,solenoidNum) => {
        this.view.displayTableau(rows,tankType,tankList,lineList,valcoNum,solenoidNum)
    }
    /**
     * This function links the valcoNum, solenoidNum changes in the storage with the View valcoNum, solenoidNum displaying.
     * @see view.displayValcoSolenoidNum
     */
    onValcoSolenoidNumChanged = (valcoNum,solenoidNum) => {
        this.view.displayValcoSolenoidNum(valcoNum,solenoidNum)
    }

    onImageChanged = () => {
        this.view.displayStationImage()
    }

}