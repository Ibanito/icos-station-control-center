#!/usr/bin/python3
print ("Content-Type: text/html\n")

# This script command the Valvo position
# It reads the desired position on a form from the Manula.php srcipt.
# Then it send the command to the valco by a serial communication

# Import modules
try:
    import cgi, cgitb
    import serial
except ImportError:
    print('Modules not impoted')

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Read the radiobuttons states from fields
if form.getvalue('valvesPositions'):
   desiredValvePosition = form.getvalue('valvesPositions')
else:
   desiredValvePosition = "Not set"
   
print ("<h2> Desired valve position :  %s</h2>" % desiredValvePosition)

# Control Valco
ser = serial.Serial(port='/dev/ttyUSB0', baudrate=9600, bytesize=8, parity='N', stopbits=1, timeout=1, xonxoff=1, rtscts=0)  # open serial port
print(ser.name)         # check which port was really used
print(ser.is_open)


if desiredValvePosition == 'valvePosition01':
    ser.write('GO1\r'.encode('utf-8'))
elif desiredValvePosition == 'valvePosition02':
    ser.write('GO2\r'.encode('utf-8'))
elif desiredValvePosition == 'valvePosition03':
    ser.write('GO3\r'.encode('utf-8'))
elif desiredValvePosition == 'valvePosition04':
    ser.write('GO4\r'.encode('utf-8'))
elif desiredValvePosition == 'valvePosition05':
    ser.write('GO5\r'.encode('utf-8'))
elif desiredValvePosition == 'valvePosition06':
    ser.write('GO6\r'.encode('utf-8'))

ser.close()             # close port
print(ser.is_open)
