#!/usr/bin/python3
print ("Content-Type: text/html\n")
var = 4
print ("Hello World")
print (var)


# LED BLINKING
import RPi.GPIO as GPIO #Importe la bibliothèque pour contrôler les GPIOs

GPIO.setmode(GPIO.BOARD) #Définit le mode de numérotation (Board)
GPIO.setwarnings(False) #On désactive les messages d'alerte

LED = 7 #Définit le numéro du port GPIO qui alimente la led

GPIO.setup(LED, GPIO.OUT) #Active le contrôle du GPIO

state = GPIO.input(LED) #Lit l'état actuel du GPIO, vrai si allumé, faux si éteint

if state : #Si GPIO allumé
    GPIO.output(LED, GPIO.LOW) #On l’éteint
else : #Sinon
    GPIO.output(LED, GPIO.HIGH) #On l'allume
    
      
# Control Valco
    
import serial
ser = serial.Serial(port='/dev/ttyUSB0', baudrate=9600, bytesize=8, parity='N', stopbits=1, timeout=1, xonxoff=1, rtscts=0)  # open serial port
print(ser.name)         # check which port was really used
print(ser.is_open)

ser.write('GO2\r'.encode('utf-8'))     # write a string
print(ser.read(8))

ser.write('GO3\r'.encode('utf-8'))     # write a string
print(ser.read(8))

ser.close()             # close port
print(ser.is_open)