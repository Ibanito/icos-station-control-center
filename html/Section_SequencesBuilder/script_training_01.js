
/*
Test GitLab 06 - useless comment
*/

/**
 * @class Model
 *
 * Model class of the MVC pattern.
 * Manages the data of the application.
 */
class Model {
    /**
     * Create the MVC Model instance
     * The sequences object contains all of the sequences. This object is created here.
     * 
     * TODO: add comments about the tempSequence object. Is this object still usefull ?
     * 
     * @constructor
     */
    constructor() {
        this.sequences = JSON.parse(localStorage.getItem('sequences')) || []
        this.tempSequence = JSON.parse(localStorage.getItem('tempSequence')) || []
    }

    /**
     * When the model sequences list has changed, the changes has to be seen on the View.
     * This function links the view with the model to upadte the sequences list.
     * 
     * @param callback
     */
    bindSequencesListChanged(callback) {
        this.onSequencesListChanged = callback
    }

    /**
     * WARNING: uselesse function to be removed
     * TODO: remove this function
     * 
     * @deprecated
     * @private
     * @param {object} sequence
     */
    _commitTempSequence(sequence) {
        //this.onSequencesListChanged(sequences)
        localStorage.setItem('tempSequence', JSON.stringify(sequence))
        console.log(sequence)
    }

    /**
     * The sequences object contains all of the sequences.
     * This function receives a new sequences object and then replace the old sequences object with the new one.
     * This operation request a conversion from JavaSript object to JSON.
     * 
     * @private
     * @param {object} sequence
     */
    _commitSequence(sequences) {
        this.onSequencesListChanged(sequences)
        localStorage.setItem('sequences', JSON.stringify(sequences))
        //console.log(sequences)
    }

    /**
     * Crate a new JSON object that represents a new sequence.
     * The new sequence ID is defined by the next sequences number from the sequences counter.
     * When the new sequence object is created, it is pushed inside a bigger object named sequences, that contains all of the sequences.
     * Then the updated sequences object is sended to an other object in order to be saved.
     * 
     * @see _commitSequence
     */
    createSequence() {
        var _id = this.sequences.length > 0 ? this.sequences[this.sequences.length - 1].id + 1 : 1
        var _name = "New Sequence " + _id
        var _sequenceType = 'Ambient'
        var _numberOfCycles = 1
        var _cycleDays = 1
        var _cyclesHours = 0
        var _cyclesMinutes = 0
        var _duration = 0

        var sequence = {
            id: _id,
            name: _name,
            sequenceType: _sequenceType,
            numberOfCycles: _numberOfCycles,
            cycleDays: _cycleDays,
            cyclesHours: _cyclesHours,
            cyclesMinutes: _cyclesMinutes,
            duration: _duration,
            steps: [{ id: 0, position: 4, duration: 1800 }],
        }

        this.sequences.push(sequence)

        this._commitSequence(this.sequences)
    }

    /**
     * The sequences object contains all of the sequences, refered by an ID.
     * This function get a particular sequence ID, find it inside the sequences and delete it.
     * Then the updated sequences object is sended to an other object in order to be saved. 
     * 
     * @see _commitSequence
     */
    deleteSequence(sequenceId) {
        this.sequences = this.sequences.filter(sequence => sequence.id !== sequenceId)

        this._commitSequence(this.sequences)
    }

    /**
     * The sequences object contains all of the sequences, refered by an ID.
     * This function get a particular sequence ID, find it inside the sequences and replace it.
     * The replacement sequence is passed as parameter.
     * 
     * At the end of the process the sequence is sended to the server in order to be saved.
     * 
     * @param {number} sequenceId
     * @param {object} sequence
     * @see _sendSequenceToServer
     */
    updateSequence(sequenceId, sequence) {
        console.log('Update sequence')
        //console.log(sequence)

        this.sequences = this.sequences.filter(sequence => sequence.id !== sequenceId)
        this.sequences.push(sequence)

        this._commitSequence(this.sequences)

        console.log(sequence)
        this._sendSequenceToServer(sequenceId)
    }

    /**
     * This function calls a php script in order to save a sequence to the server.
     * The sequence is extrated from the sequences object with the sequence ID.
     * Then the sequence object is converted into a string and sended to a php script.
     * 
     * @private
     * @param {number} sequenceId
     */
    _sendSequenceToServer(sequenceId) {
        console.log('sendSequenceToServer')
        console.log(sequenceId)

        console.log(this.sequences)
        const sequence = this.sequences.filter(sequence => sequence.id == sequenceId)

        console.log(sequence)
        const jsonString = JSON.stringify(sequence)
        const xhr = new XMLHttpRequest()

        xhr.open('POST', '../Bridge/FileReceiver.php')
        xhr.setRequestHeader('Content-Type', 'application/json')
        xhr.send(jsonString)
        console.log(jsonString)
    }
}


//##################################################################################################################
//##################################################################################################################
//##################################################################################################################


/**
 * @class View
 * 
 * View class of the MVC pattern.
 * Visual representation of the model.
 */
class View {
    /**
     * Create the MVC View instance
     * The main DOM squeletton is created here.
     * 
     * @constructor
     */
    constructor() {
        // Main View blocks from the HTML file
        this.sequenceInBuldingHeader = this.getElement('#sequenceInBuldingHeader')
        this.sequenceInBuldingStepsList = this.getElement('#sequenceInBuldingStepsList')
        this.sequenceInBuldingFooter = this.getElement('#sequenceInBuldingFooter')

        this.app = this.getElement('#root')

        //this._initLocalListeners()

        // Sequence builder region
        this.appBuilding = this.getElement('#sequenceBulding')


        // Sequences list region

        this.addNewStepButton = this.createElement('button', 'createSequence')
        this.addNewStepButton.textContent = 'Create a new sequence'
        //this.form.append(this.addNewStepButton)
        this.app.append(this.addNewStepButton)



        this.SequencesList = this.createElement('ul', 'todo-list')
        this.app.append(this.SequencesList)

        this.InBuildingSequenceHeader = this.createElement('div')
        this.appBuilding.append(this.InBuildingSequenceHeader)

        this.stepsList = this.createElement('ul', 'stepsList')
        this.sequenceInBuldingStepsList.append(this.stepsList)

    }

    /**
     * TODO: delete this function
     * WARNING: useless function
     * 
     * @deprecated
     * @private
     */
    _resetInput() {
        this.input.value = ''
    }

    /**
     * Add an element to the DOM and return the created DOM element
     *
     * @private
     * @param {string} tag
     * @param {string} className
     * @return {element} element
     */
    createElement(tag, className) {
        const element = document.createElement(tag)
        //const element = document.getElementById('mainJsContener')[0].createElement(tag)

        if (className) element.classList.add(className)

        return element
    }

    /**
     * Get an element from the DOM
     *
     * @private
     * @param {string} selector
     * @return {element} element
     */
    getElement(selector) {
        const element = document.querySelector(selector)
        //const element = document.getElementById('mainJsContener').querySelector(selector)

        return element
    }

    /**
     * All of the sequences are displayed on the left of the screen.
     * The sequence are in the sequences object. This function get this object, read it and then displays the sequences one by one.
     *
     * @param {object} sequences
     */
    displaySequences(sequences) {
        // Delete all nodes


        while (this.SequencesList.firstChild) {
            this.SequencesList.removeChild(this.SequencesList.firstChild)
        }

        // Show default message
        if (sequences.length === 0) {
            const p = this.createElement('p')
            p.textContent = 'No sequences'
            this.SequencesList.append(p)
        } else {
            // Create nodes
            sequences.forEach(sequences => {

                const sequenceBlockContainer = this.createElement('div', 'sequenceBlockContainer')
                const sequenceText = this.createElement('div', 'sequenceText')
                const sequenceButton = this.createElement('div', 'sequenceButton')



                const li = this.createElement('li')
                li.id = sequences.id

                // Sequence name
                const p = this.createElement('div', 'textSequenceName')
                p.textContent = sequences.name

                const p3 = this.createElement('div')
                p3.textContent = 'Type: ' + sequences.sequenceType

                const p2 = this.createElement('div')
                if (sequences.duration != null) {
                    p2.textContent = 'duration: ' + sequences.duration.toString(10)
                }

                sequenceText.append(p)
                sequenceText.append(p3)
                sequenceText.append(p2)
                sequenceBlockContainer.append(sequenceText)

                // Edit button
                const editButton = this.createElement('button', 'editSequence')
                editButton.textContent = 'Edit'
                sequenceButton.append(editButton)

                sequenceBlockContainer.append(sequenceButton)

                // Append nodes
                li.append(sequenceBlockContainer)

                this.SequencesList.append(li)
            })
        }
    }

    /**
     * When a sequence id editted, it appears on the right of the screen.
     * First af all, the sequence is extracted from the sequnces, with its ID.
     * Then a DOM is created according to the sequence data.
     *
     * @param {object} sequences
     * @param {number} sequenceId
     */
    loadSequenceToEdit(sequences, sequenceId) {

        var _sequence = sequences.filter(sequence => sequence.id == sequenceId)[0]
        var _steps = _sequence.steps

        // Footer : display a button to add a step
        // Footer : add a button
        // delete all nodes
        while (this.sequenceInBuldingFooter.firstChild) {
            this.sequenceInBuldingFooter.removeChild(this.sequenceInBuldingFooter.firstChild)
        }

        const sequenceFooterButtons = this.createElement('div', 'sequenceFooterButtons')
        const addStepButton = this.createElement('button', 'addStep')
        addStepButton.textContent = 'Add a step'
        sequenceFooterButtons.append(addStepButton)
        this.sequenceInBuldingFooter.append(sequenceFooterButtons)


        // Header : display sequence main configuration
        // Delete all nodes
        while (this.sequenceInBuldingHeader.firstChild) {
            this.sequenceInBuldingHeader.removeChild(this.sequenceInBuldingHeader.firstChild)
        }

        // Header - view skeleton
        const sequenceHeader = this.createElement('div', 'sequenceHeader')

        const sequenceHeaderButtons = this.createElement('div', 'sequenceHeaderButtons')
        const sequenceHeaderData = this.createElement('div', 'sequenceHeaderData')

        const sequenceHeaderName = this.createElement('div', 'sequenceHeaderName')
        const sequenceHeaderType = this.createElement('div', 'sequenceHeaderType')
        const sequenceHeaderCyclesNumber = this.createElement('div', 'sequenceHeaderCyclesNumber')
        const sequenceHeaderCycleDuration = this.createElement('div', 'sequenceHeaderCycleDuration')
        const sequenceHeaderDuration = this.createElement('div', 'sequenceHeaderDuration')

        sequenceHeaderData.append(sequenceHeaderName)
        sequenceHeaderData.append(sequenceHeaderType)
        sequenceHeaderData.append(sequenceHeaderCyclesNumber)
        sequenceHeaderData.append(sequenceHeaderCycleDuration)
        sequenceHeaderData.append(sequenceHeaderDuration)

        sequenceHeader.append(sequenceHeaderData)
        sequenceHeader.append(sequenceHeaderButtons)

        this.sequenceInBuldingHeader.append(sequenceHeader)

        sequenceHeader.id = _sequence.id

        // Header - save button
        const saveButton = this.createElement('button', 'saveSequence')
        saveButton.textContent = 'Save changes'
        sequenceHeaderButtons.append(saveButton)

        // Header - delete button
        const deleteButton = this.createElement('button', 'deleteSequence')
        deleteButton.textContent = 'Delete sequence'
        sequenceHeaderButtons.append(deleteButton)

        // Header - input sequence name
        this.formSequenceHeader = this.createElement('form')
        this.inputSequenceName = this.createElement('input')
        this.inputSequenceName.type = 'text'
        this.inputSequenceName.value = _sequence.name
        this.formSequenceHeader.append(this.inputSequenceName)

        const p3 = this.createElement('p')
        p3.textContent = "Name"
        sequenceHeaderName.append(p3)
        sequenceHeaderName.append(this.formSequenceHeader)

        // Header - sequence type
        // text
        const p4 = this.createElement('p')
        p4.textContent = "Type "
        sequenceHeaderType.append(p4)
        // form
        this.sequencesTypesMenu = this.createElement('select', 'sequencesTypes')

        var option1 = document.createElement("option")
        option1.value = "V1"
        option1.innerHTML = "Ambient"

        var option2 = document.createElement("option")
        option2.value = "V2"
        option2.innerHTML = "Calibration"

        var option3 = document.createElement("option")
        option3.value = "V3"
        option3.innerHTML = "Intercomparaison"

        var option4 = document.createElement("option")
        option4.value = "V4"
        option4.innerHTML = "Measure"

        var option5 = document.createElement("option")
        option5.value = "V5"
        option5.innerHTML = "Standby"

        this.sequencesTypesMenu.appendChild(option1)
        this.sequencesTypesMenu.appendChild(option2)
        this.sequencesTypesMenu.appendChild(option3)
        this.sequencesTypesMenu.appendChild(option4)
        this.sequencesTypesMenu.appendChild(option5)

        sequenceHeaderType.append(this.sequencesTypesMenu)

        // Header - sequence number of cycles
        const p5 = this.createElement('p')
        p5.textContent = 'Number of cycles'
        sequenceHeaderCyclesNumber.append(p5)

        this.formNumberOfCycles = this.createElement('form')
        this.inputNumberOfCycles = this.createElement('input')
        this.inputNumberOfCycles.type = 'text'
        this.inputNumberOfCycles.value = _sequence.numberOfCycles
        this.formNumberOfCycles.append(this.inputNumberOfCycles)
        sequenceHeaderCyclesNumber.append(this.formNumberOfCycles)

        // Header - One cycle duration
        const p6 = this.createElement('p')
        p6.textContent = 'One cycle duration: '
        sequenceHeaderCycleDuration.append(p6)

        // Header - One cycle duration
        const p7 = this.createElement('p')
        p7.textContent = 'Sequence duration: '
        sequenceHeaderDuration.append(p7)

        // --- Steps listing : ---
        // Delete all nodes
        while (this.stepsList.firstChild) {
            this.stepsList.removeChild(this.stepsList.firstChild)
        }

        // Show default message
        if (_sequence.length === 0) {
            const p = this.createElement('p')
            p.textContent = 'Select a sequence to edit or create one.'
            this.InBuildingSequence.append(p)
        } else {
            // Create nodes
            _steps.forEach(steps => {
                var _id = steps.id
                var _position = steps.position
                var _duration = steps.duration
                this.addStep(_id, _position, _duration)
            })
        }
    }

    /**
     * This function creates a new sequence object by reding the DOM data.
     * At the end of the process, the new sequence is sended to the Model to be saved.
     * A pop-up windows indicates the sequence has been saved.
     * 
     * @see bindUpdateSequence
     * @private
     * @param {} handler
     * @return jsonTempSequence
     * 
     * TODO: find the handler type
     */
    updateSequence(id) {

        var jsonTempSequence = new Object();
        var jsonTempSteps = new Array();

        // Get sequence name
        var _sequenceName = document.getElementsByClassName('sequenceHeaderData')[0].getElementsByClassName('sequenceHeaderName')[0].getElementsByTagName('form')[0].getElementsByTagName('input')[0].value

        // Get sequence id
        var _sequenceId = Number(document.getElementsByClassName('sequenceHeader')[0].id)

        // Get the unmber of cycles
        var _cyclesNumber = Number(document.getElementsByClassName('sequenceHeaderData')[0].getElementsByClassName('sequenceHeaderCyclesNumber')[0].getElementsByTagName('form')[0].getElementsByTagName('input')[0].value)

        // Get sequence steps, positions and durations
        var ul = document.getElementsByClassName('stepsList')[0]
        var items = ul.getElementsByTagName("li");
        for (var i = 0; i < items.length; ++i) {

            var _id = items[i].id
            var _position = items[i].getElementsByClassName('stepBlocPositionForm')[0].getElementsByTagName('form')[0].getElementsByTagName('input')[0].value
            var _duration = items[i].getElementsByClassName('stepBlocDurationForm')[0].getElementsByTagName('form')[0].getElementsByTagName('input')[0].value

            var jsonTempStep = new Object();
            jsonTempStep.id = Number(_id)
            jsonTempStep.position = Number(_position)
            jsonTempStep.duration = Number(_duration)

            jsonTempSteps.push(jsonTempStep)
        }

        // Fill the new sequence with the data just founded
        jsonTempSequence.id = _sequenceId
        jsonTempSequence.name = _sequenceName
        jsonTempSequence.numberOfCycles = _cyclesNumber
        jsonTempSequence.steps = jsonTempSteps


        //console.log("Temp sequence:")
        //console.log(jsonTempSequence)

        //handler(id, jsonTempSequence)

        // Show a pop-up window to indicate the sequence has been saved
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'The sequence has been saved',
            showConfirmButton: false,
            timer: 1500
        })

        return jsonTempSequence
    }

    /**
     * This function removes the sequence from the DOM by deleting all of the elements.
     * The event calls the model and delete the sequence from the local storage.
     * 
     * @see model.deleteSequence
     * @param {} handler
     * 
     * TODO: find the handler type
     */
    deleteSequence(id) {
        // Refresh the View :
        // Steps header - delete all nodes
        while (this.sequenceInBuldingHeader.firstChild) {
            this.sequenceInBuldingHeader.removeChild(this.sequenceInBuldingHeader.firstChild)
        }
        // Steps Footer - delete all nodes
        while (this.sequenceInBuldingFooter.firstChild) {
            this.sequenceInBuldingFooter.removeChild(this.sequenceInBuldingFooter.firstChild)
        }

        // Steps listing - delete all nodes
        while (this.stepsList.firstChild) {
            this.stepsList.removeChild(this.stepsList.firstChild)
        }

        console.log("delete sequence")
        console.log(event.target.parentElement.parentElement.id)
        console.log(id)

    }

    /**
     * When the "Add a step" button is clicked, this function add the needed elements to the DOM in order to see the new step.
     * 
     * @see bindAddStep
     * @private
     * @param {number} id
     * @param {number} position
     * @param {number} duration
     */
    addStep(id, position, duration) {
        // Step block - skeleton
        const stepBlocInputs = this.createElement('div', 'stepBlocInputs')
        const stepBlocContainer = this.createElement('div', 'stepBlocContainer')
        const stepBlocPosition = this.createElement('div', 'stepBlocPosition')
        const stepBlocPositionText = this.createElement('div', 'stepBlocPositionText')
        const stepBlocPositionForm = this.createElement('div', 'stepBlocPositionForm')
        const stepBlocDuration = this.createElement('div', 'stepBlocDuration')
        const stepBlocDurationText = this.createElement('div', 'stepBlocDurationText')
        const stepBlocDurationForm = this.createElement('div', 'stepBlocDurationForm')
        const stepBlockButtons = this.createElement('div', 'stepBlockButtons')

        stepBlocPosition.append(stepBlocPositionText, stepBlocPositionForm)
        stepBlocDuration.append(stepBlocDurationText, stepBlocDurationForm)
        stepBlocInputs.append(stepBlocPosition, stepBlocDuration)
        stepBlocContainer.append(stepBlocInputs)
        stepBlocContainer.append(stepBlockButtons)

        // Step block - position form
        this.formPosition = this.createElement('form')
        this.inputPosition = this.createElement('input')
        this.inputPosition.type = 'text'
        this.inputPosition.value = position
        this.formPosition.append(this.inputPosition)
        stepBlocPositionForm.append(this.formPosition)

        // Step block - duration form
        this.formDuration = this.createElement('form')
        this.inputDuration = this.createElement('input')
        this.inputDuration.type = 'text'
        this.inputDuration.value = duration
        this.formDuration.append(this.inputDuration)
        stepBlocDurationForm.append(this.formDuration)

        const li = this.createElement('li')
        li.id = id

        // Step block - position text
        const p = this.createElement('p')
        p.textContent = "Position "
        stepBlocPositionText.append(p)

        // Step block - duration text
        const p2 = this.createElement('p')
        p2.textContent = "Duration "
        stepBlocDurationText.append(p2)

        // Step block buttons
        const deleteStepBlock = this.createElement('button', 'deleteStepBlock')
        deleteStepBlock.textContent = 'Delete'
        stepBlockButtons.append(deleteStepBlock)

        // Append nodes
        li.append(stepBlocContainer)
        this.sequenceInBuldingStepsList.getElementsByTagName('ul')[0].append(li)

        // Draw a line to link the blocks togethers
        const line = this.createElement('hr')
        li.append(line)
    }

    /**
    * This function links the "Create Sequence" button to the MVC Controller.
    *
    * @param {} handler
    * 
    * TODO: find the handler type
    */
    bindCreateSequence(handler) {
        //this.form.addEventListener('click', event => {
        this.app.addEventListener('click', event => {
            if (event.target.className === 'createSequence') {
                const id = parseInt(event.target.parentElement.id)

                handler(id)
                console.log("create sequence")
            }
        })
    }

    /**
     * This function links the "Edit Sequence" button to the MVC Controller.
     *
     * @param {h} handler
     * 
     * TODO: find the handler type
     */
    bindEditSequence(handler) {
        this.SequencesList.addEventListener('click', event => {
            if (event.target.className === 'editSequence') {
                const id = parseInt(event.target.parentElement.parentElement.parentElement.id)
                //console.log(event.target)
                //console.log(event.target.parentElement)
                handler(id)
            }
        })
    }

    /**
     * This function links the "Save Sequence" button to the MVC Controller.
     * 
     * @see model.updateSequence
     * @private
     * @param {} handler
     * 
     * TODO: find the handler type
     */
    bindUpdateSequence(handler) {
        this.appBuilding.addEventListener('click', event => {
            if (event.target.className === 'saveSequence') {
                const id = parseInt(event.target.parentElement.parentElement.id)

                const jsonTempSequence = this.updateSequence(id)

                handler(id, jsonTempSequence)
            }
        })
    }

    /**
     * This function links the "Delete Sequence" button to the MVC Controller.
     * Futhermore it deletes the sequence by calling an other function.
     * 
     * @see deleteSequence
     * @param {} handler
     * 
     * TODO: find the handler type
     */
    bindDeleteSequence(handler) {
        this.sequenceInBuldingHeader.addEventListener('click', event => {
            if (event.target.className === 'deleteSequence') {
                const id = parseInt(event.target.parentElement.parentElement.id)
                handler(id)

                this.deleteSequence(id)
            }
        })
    }

    /**
     * This function links the "Add step" button to the MVC Controller.
     * Futher more it calls an event and add a step by calling an other function.
     *
     * @private
     * @see addStep
     * @param {} handler
     * 
     * TODO: find the handler type
     */
    bindAddStep(handler) {
        this.sequenceInBuldingFooter.addEventListener('click', event => {
            if (event.target.className === 'addStep') {

                // New step ID definition
                var _id = document.getElementsByClassName('stepsList')[0].getElementsByTagName('li').length

                this.addStep(_id, 8, 42)
            }
        })
    }

    /**
     * This function links the "Delete step" button to the MVC Controller.
     * Futhermore:
     * When the "Delete step" button is clicked, this function removes the step elements from the DOM.
     *
     * @private
     * @param {} handler
     * 
     * TODO: find the handler type
     */

    //TODO: split the function, put "deleteStep" outside.
    bindDeleteStep(handler) {
        this.sequenceInBuldingStepsList.addEventListener('click', event => {
            if (event.target.className === 'deleteStepBlock') {
                var _stepBlockToRemove = event.target.parentElement.parentElement.parentElement
                _stepBlockToRemove.parentElement.removeChild(_stepBlockToRemove)
            }
        })
    }
}



//##################################################################################################################
//##################################################################################################################
//##################################################################################################################

/**
 * @class Controller
 * 
 * Controller class of the MVC pattern
 * Links the user input and the view output.
 *
 */
class Controller {
    /**
     * Create the MVC Conttroller instance
     * All of the button view events are binded here.
     * 
     * @constructor
     * @param model
     * @param view
     */
    constructor(model, view) {
        this.model = model
        this.view = view

        // Explicit this binding
        this.model.bindSequencesListChanged(this.onSequencesListChanged)
        this.view.bindDeleteSequence(this.handleDeleteSequence)
        this.view.bindEditSequence(this.handleEditSequence)
        this.view.bindUpdateSequence(this.handleUpdateSequence)
        this.view.bindCreateSequence(this.handleCreateSequence)
        this.view.bindAddStep(this.handleAddStep)
        this.view.bindDeleteStep(this.handleDeleteStep)

        // Display initial todos
        this.onSequencesListChanged(this.model.sequences)
    }

    /**
     * This function links the Model sequences changes in the storage with the View sequences displaying.
     * @see view.displaySequences
     */
    onSequencesListChanged = sequences => {
        this.view.displaySequences(sequences)
    }

    /**
     * This function links the View deleting sequences with the Model sequences.
     * @see model.deleteSequence
     */
    handleDeleteSequence = id => {
        this.model.deleteSequence(id)
    }

    /**
     * This function links the Model sequences in the storage with the View in order to display the sequences.
     * @see view.loadSequenceToEdit
     */
    handleEditSequence = id => {
        //console.log("Controller: Edit Sequence")
        //console.log(id)
        this.view.loadSequenceToEdit(this.model.sequences, id)
    }

    /**
     * This function links the View created sequences with the Model stored sequences.
     * @see model.createSequence
     */
    handleCreateSequence = () => {
        this.model.createSequence()
    }

    /**
     * This function links the View updated sequences with the Model stored sequences.
     * @see model.updateSequence
     */
    handleUpdateSequence = (id, sequence) => {
        this.model.updateSequence(id, sequence)
    }

    /**
     * This function get the button's event "Add a step"
     * 
     * TODO: improve comments
     */
    handleAddStep = () => {
        console.log("handleAddStep")
    }

    /**
     * This function get the button's event "Add a step"
     * 
     * TODO: improve comments
     */
    handleDeleteStep = () => {
        console.log("handleDeleteStep")
    }
}

const app = new Controller(new Model(), new View())