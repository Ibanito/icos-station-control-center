# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 15:23:59 2020

@author: ifernand
"""


import serial
#import time

class Valcobox:
    """ Valcobox hardware management
    
    This class represents the multipositions solenoids valves hardware.
    It performs the low level serial communication between software and
    hardware.
    
    The valobox Serial/USB converter has to be linked on the COM3.
#    
    """

    #--- CONSTRUCOTR ---
    def __init__(self):
        self.ser = object()
        #self.number_of_valve_positions = 0


    #--- LOW LEVEL FUCNTIONS ---
    def open_communication(self):
        self.ser = serial.Serial(port = 'COM3', \
                                 baudrate = 9600, \
                                 bytesize = 8, \
                                 parity = 'N', \
                                 stopbits = 1, \
                                 timeout = 1, \
                                 xonxoff = 1, \
                                 rtscts = 0)
        
        
    def close_communication(self):
        self.ser.close()
        
             
    #--- HIGH LEVEL FUCNTIONS ---
    def command_valve(self, valve_position):
        """Command the valve through the serial port to reach the position"""
        self.open_communication()        
        serial_command = 'GO' + str(valve_position) + '\r'
        try:
            self.ser.write(serial_command.encode('utf-8'))
        except:
            self.ser.close()
            print("ERROR")
        self.close_communication()
        
        
#    def get_the_number_of_valves_positions():
#        pass
        
        

#--- SAMPLE: HOW TO USE THE CLASS --- 
#valcobox = Valcobox()
#valcobox.command_valve(3)
#time.sleep(3)
#valcobox.command_valve(4)