# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 13:39:02 2020

@author: ifernand
"""

import time
from Class_Sequences_Manager import Sequences_Manager
from Class_Valcobox import Valcobox
from Class_Bdd_Csv import Bdd_Csv


valcobox = Valcobox()
database = Bdd_Csv()
sequences_manager = Sequences_Manager("Sequences_process.xml")


def main():

    sequences_manager.start()
    
    desired_valve_position = 0
    current_valve_position = 0
    
    while True:
        desired_valve_position = sequences_manager.valve_position_to_go
        
        if current_valve_position is not desired_valve_position:
            current_valve_position = desired_valve_position
            
#            valcobox.command_valve(desired_valve_position)
            
            # Update the BDD
            database.add_data_to_file(current_valve_position)

        time.sleep(1)

    sequences_manager.flag_kill_thread = True


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Program end")
        sequences_manager.flag_kill_thread = True
        
        
        
# if no file have been changed, loop:
#        t_end = time.time() + 20
#        while time.time() < t_end: