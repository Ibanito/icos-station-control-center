# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 10:41:28 2020

@author: ifernand
"""

import csv
from datetime import datetime

class Bdd_Csv:
    
    #--- CONSTRUCTOR ---
    def __init__(self):
        pass
    
            
    #---FUNCTIONS ---    
    def add_data_to_file(self, valve_position):
        
        mydate = datetime.now()
        date_on_str_format = datetime.strftime(mydate, '%Y, %m, %d, %H, %M, %S')

        data_line = (["Valve position", str(valve_position), "Date", date_on_str_format])
        
        with open('Valves_positions.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(data_line)
             
            
            
#--- SAMPLE: HOW TO USE THE CLASS ---
#bdd_csv = Bdd_Csv()
#bdd_csv.add_data_to_file(85)