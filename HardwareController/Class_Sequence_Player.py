# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 16:37:08 2020

@author: ifernand
"""

import threading
import time
from datetime import datetime
import xml.etree.ElementTree as ET


class Sequence_Player(threading.Thread):
    """Play the sequence without stopping and update the outputs.
    
    This class implement a sequence player. The sequence is stored inside a file.
    The file is opened and read. Then the class acts like a thread and play the
    file without stopping.
    The player read the vavle position, wait a certain duration and start again
    until the file's end is not reached.
    The valve position is stored inside a public variable. An other class checks
    constatntly this variable to command the valve.
    
    """
    
    #--- CONSTRUCOTR ---
    def __init__(self, sequence_file_path):
        threading.Thread.__init__(self)
        self.flag_kill_thread = False
        self.sequence_file_path = sequence_file_path
        
        # Sequence meta data:
        self.sequence_name = ""
        self.sequence_is_activated = False
        self.time_to_activate = 0
        self.sequence_duration = 0
        self.is_a_background_sequence = False
        self.valve_position = 0
        self.number_of_sequence_cycles = 1
        
        self.sequence_root = object()
        
        # Time management :
        self.cycles_days = 1
        self.cycles_hours = 12
        self.cycles_minutes = 30
        self.date_of_next_activation = 0
        
        self.TIME_ZONE = 1

        
    #--- LOW LEVEL FUCNTIONS ---
    
    def read_xml_file(self, file_path):
        tree = ET.ElementTree()
        tree.parse(file_path)
        root = tree.getroot()
        return root
    
    
    def today_in_unix_time(self, time_zone):
        """Compute the next activation time in seconds. Time 0 is the 01/01/1970"""    
        today_date = datetime.today()
        today_in_unix_time = today_date.timestamp() + time_zone * 3600               
        return(today_in_unix_time)
        
           
    def today_in_unix_time_truncated_at_days(self, time_zone):
        today_date = datetime.today()
        today_truncated = today_date.replace(hour = 0, minute = 0, second = 0, microsecond = 0)
        today_in_unix_time_truncated = today_truncated.timestamp() + time_zone * 3600  
        return today_in_unix_time_truncated
       

    def hours_and_minutes_to_seconds(self, hours, minutes):
        """Compute the daily activation time in seconds."""     
        return(hours * 3600 + minutes * 60)

    
    #--- HIGH LEVEL FUCNTIONS ---    
    
    def compute_next_date_of_activation(self, days, hours, minutes, time_zone):
        """Return the next date of activation in seconds. Time 0 is the 01/01/1970"""  
        next_day_of_activation_in_seconds = self.today_in_unix_time_truncated_at_days(time_zone) + days * 86400
        time_in_seconds = self.hours_and_minutes_to_seconds(hours, minutes)  
        next_date_of_activation = next_day_of_activation_in_seconds + time_in_seconds
        
        print("Name: ", self.sequence_file_path, " Next activation: ", next_date_of_activation)
        
        return next_date_of_activation
        
    
    def setup_sequence(self):
        """Read the file - compute next time to execute - update object attributs"""
        # Read the file
        self.sequence_root = self.read_xml_file(self.sequence_file_path)
    
        # Update the attributs by reading the file
        for configuration in self.sequence_root.iter('configuration'):   
            self.sequence_name = configuration.find('name').text
            self.number_of_sequence_cycles = configuration.find('number_of_cycles').text
            self.cycles_days = int(configuration.find('cycle_days').text)
            self.cycles_hours = int(configuration.find('cycle_hours').text)
            self.cycles_minutes = int(configuration.find('cycle_minutes').text)
            
        # Compute the next execution time
        self.date_of_next_activation = self.compute_next_date_of_activation(days = self.cycles_days, \
                                                                            hours = self.cycles_hours, \
                                                                            minutes = self.cycles_minutes, \
                                                                            time_zone = self.TIME_ZONE)
                                   
            
    def play_sequence(self):
        """Read and perform the file step by step"""
        
        # -> Background sequence bahavior:
        if self.is_a_background_sequence is True:
            for step in self.sequence_root.iter('step'):          
                self.valve_position = step.find('position').text
                time.sleep(int(step.find('duration').text))
        
        # -> Standart sequence bahavior:
        else:
            if self.sequence_is_activated is True:                      
                cycles_to_run = int(self.number_of_sequence_cycles)
                while cycles_to_run > 0:
                    
                    print("cycle", cycles_to_run)
                    
                    for step in self.sequence_root.iter('step'):          
                        self.valve_position = step.find('position').text
                        time.sleep(int(step.find('duration').text))
                   
                    cycles_to_run -= 1                
            
                # When the sequence is over, desactivate it
                self.sequence_is_activated = False
                # Then compute the next execution time
                self.date_of_next_activation = self.compute_next_date_of_activation(days = self.cycles_days, \
                                                                    hours = self.cycles_hours, \
                                                                    minutes = self.cycles_minutes, \
                                                                    time_zone = self.TIME_ZONE)
                
#                self.date_of_next_activation = 1580897940.0
                
            # If the sequence is not activated, check the activation date to
            # reactive it on time
            else:                
                if self.today_in_unix_time(self.TIME_ZONE) > self.date_of_next_activation:
                    self.sequence_is_activated = True
                
                else:
                    time.sleep(10)
                    
                
    #--- FIRST FUNCTION TO BE EXECUTED ---
    def run(self):
        self.setup_sequence()
        
        # Is the file well formed ?
        # If not retry or send error        
        
        while True:
            self.play_sequence()
            
            if self.flag_kill_thread is True:
                break
        
        
#--- SAMPLE: HOW TO USE THE CLASS --- 
#thread_1 = Sequence_Player(sequence_file_path = "SequenceTest01.xml")
#thread_2 = Sequence_Player(sequence_file_path = "SequenceTest02.xml")
#thread_1.daemon = True
#thread_1.start()
#thread_2.start()

#print(thread_1.compute_next_date_of_activation(thread_1.cycles_days, \
#                                         thread_1.cycles_hours, \
#                                         thread_1.cycles_minutes))

