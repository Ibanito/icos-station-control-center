# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 14:06:23 2020

@author: Iban FERNANDEZ
"""
import time
import xml.etree.ElementTree as ET
import datetime
from Class_Sequence_Player import Sequence_Player
#from Class_Valcobox import Valcobox
import threading
#import sys


print("Script class sequences manager")


class Sequences_Manager(threading.Thread):
    """ Manage all the sequences players and commands the electrovalve.
    
    This class Launch all of the sequences player and choose wich of them to 
    listen. Then it commands the electrovalve.
    
    """
    
    #--- CONSTRUCOTR ---
    def __init__(self, sequences_process_file_path):
        threading.Thread.__init__(self)
        self.flag_kill_thread = False
        self.sequences_process_file_path = sequences_process_file_path
#        self.valcobox = Valcobox()              
        self.sequences_players_list = list()
        self.sequence_to_be_listened = object()
        self.background_sequence = object()
        self.current_valve_position = 0
        self.valve_position_to_go = 0
        
                  
    #--- LOW LEVEL FUCNTIONS ---
    def read_xml_file(self, file_path):
        tree = ET.ElementTree()
        tree.parse(file_path)
        root = tree.getroot()
        return root
        
        
    def get_time_in_seconds(self):
        time = datetime.datetime.now()
        seconds = time.hour * 360 + time.minute * 60 + time.second       
        return seconds
       
    
    #--- HIGH LEVEL FUCNTIONS ---
    def setup_sequences_players(self):
        """ Initialise all the sequences players
        
        The sequenes players are stored inside a file. This function read the
        file and create a player object for each player read inside the file.
        A players list is created. Then the players are updated with additionnals
        parameters stored inside the file, like the priority between players.
        
        """
        sequences_process_root = self.read_xml_file("Sequences_process.xml")
        
        # Find the background sequence
        for sequence in sequences_process_root.iter('background_sequence'):
            self.background_sequence = Sequence_Player(sequence_file_path = sequence.find('file_name').text)
            self.background_sequence.is_a_background_sequence = True
            self.background_sequence.start()
        
        # Create a list with all of the playing sequences written on the file
        # And then read the sequence player priority from the file and set it on its attributs
        # Finally start the sequence player thread
        for sequence in sequences_process_root.iter('sequence'):            
            self.sequences_players_list.append(Sequence_Player(sequence_file_path = sequence.find('file_name').text))
            self.sequences_players_list[-1].sequence_priority = sequence.find('priority').text  
            self.sequences_players_list[-1].start()
            
        # Order the list by sequences priorities
        def get_priority_value(sequence_player):
            return sequence_player.sequence_priority
        self.sequences_players_list.sort(key = get_priority_value)   
            
        print("Background sequence : ", \
              self.background_sequence.sequence_file_path)
    
        for sequence_player in self.sequences_players_list:

            print("------>", \
                  "Sequence file path : ", \
                  sequence_player.sequence_file_path, \
                  "Priority : ", \
                  sequence_player.sequence_priority, \
                  "Time to activate : ", \
                  sequence_player.time_to_activate)
            
        
    def get_the_sequence_player_to_be_listened(self):
        """Choose the sequence to be listen among all of the sequences.
        
        This function choose the sequence to be listening by reading the
        sequences players list order by priorities and by choosen the first
        element that is noticed like activated. If no sequences players are
        activated then the function choose to listen the background sequence.
        
        """  
        for sequence_player in self.sequences_players_list:
            if sequence_player.sequence_is_activated is True:
                self.sequence_to_be_listened = sequence_player
                return
            
        self.sequence_to_be_listened = self.background_sequence
            
        
    def execute_the_listened_sequence(self):
        """Transmitt the commands listened in the listened sequence
        
        This function listen the listened sequence and pays attention to its valve position.
        Whenever the valve position changes, the function notes it and update
        the position the valve has to go.
        
        """           
        desired_valve_position = self.sequence_to_be_listened.valve_position
        
        if self.current_valve_position is not desired_valve_position:
            self.current_valve_position = desired_valve_position
            self.valve_position_to_go = desired_valve_position
            
            print("-> Valve position have changed. New valve position: ", self.current_valve_position)
            
   
    #--- FIRST FUNCTION TO BE EXECUTED ---     
    def run(self):
        self.setup_sequences_players()
        
        while True:
            self.get_the_sequence_player_to_be_listened()
            self.execute_the_listened_sequence()
            time.sleep(2)
#            print(datetime.datetime.now())
            
            if self.flag_kill_thread is True:
                # Stop the sequences players threads
                for sequence_player in self.sequences_players_list:
                    sequence_player.flag_kill_thread = True
                    self.background_sequence.flag_kill_thread = True
                
                # Stop himself
                break
        
 
#--- SAMPLE: HOW TO USE THE CLASS --- 
#sequence_manager = Sequences_Manager("Sequences_process.xml")
#sequence_manager.run()
